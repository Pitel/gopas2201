package cz.gopas.kalkulacka

import android.app.Application
import android.os.StrictMode
import androidx.fragment.app.FragmentManager
import com.google.android.material.color.DynamicColors

class App : Application() {
	override fun onCreate() {
		super.onCreate()
		DynamicColors.applyToActivitiesIfAvailable(this)
		FragmentManager.enableDebugLogging(BuildConfig.DEBUG)
		StrictMode.setThreadPolicy(
			StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build()
		)
		StrictMode.setVmPolicy(
			StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build()
		)
	}
}