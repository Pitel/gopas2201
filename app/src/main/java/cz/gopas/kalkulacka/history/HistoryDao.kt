package cz.gopas.kalkulacka.history

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface HistoryDao {
	@Insert
	fun add(entity: HistoryEntity)

	@Query("SELECT * FROM history ORDER BY id DESC")
	fun getAll(): Flow<List<HistoryEntity>>
}