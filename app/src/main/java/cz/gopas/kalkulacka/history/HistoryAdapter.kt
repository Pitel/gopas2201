package cz.gopas.kalkulacka.history

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class HistoryAdapter(val onClick: (Float) -> Unit) : ListAdapter<HistoryEntity, HistoryAdapter.HistoryViewHolder>(DIFF) {
	init {
		setHasStableIds(true)
	}

	override fun getItemId(position: Int) = getItem(position).id

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = HistoryViewHolder(
		LayoutInflater.from(parent.context).inflate(android.R.layout.simple_list_item_1, parent, false)
	)

	override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
		Log.d(TAG, "Bind VH $position")
		val item = getItem(position)
		holder.text.text = "${item.value}"
		holder.itemView.setOnClickListener {
			onClick(item.value)
		}
	}

	inner class HistoryViewHolder(v: View) : RecyclerView.ViewHolder(v) {
		val text: TextView = v as TextView

		init {
			Log.d(TAG, "VH init")
		}
	}

	private companion object {
		private val TAG = HistoryAdapter::class.simpleName
		private val DIFF = object : DiffUtil.ItemCallback<HistoryEntity>() {
			override fun areItemsTheSame(oldItem: HistoryEntity, newItem: HistoryEntity) =
				oldItem.id == newItem.id

			override fun areContentsTheSame(oldItem: HistoryEntity, newItem: HistoryEntity) =
				oldItem == newItem
		}
	}
}