package cz.gopas.kalkulacka.history

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.room.Room

class HistoryViewModel(app: Application) : AndroidViewModel(app) {
	val data = Room.databaseBuilder(getApplication(), HistoryDatabase::class.java, "history")
		.build()
		.historyDao()
		.getAll()

	private companion object {
		private val TAG = HistoryViewModel::class.simpleName
	}
}