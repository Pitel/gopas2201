package cz.gopas.kalkulacka.history

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import cz.gopas.kalkulacka.R
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class HistoryFragment : Fragment(R.layout.fragment_history) {
	private val viewModel by viewModels<HistoryViewModel>()

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		val recycler = view as RecyclerView
		val adapter = HistoryAdapter {
			with(findNavController()) {
				previousBackStackEntry?.savedStateHandle?.set(HIST_KEY, it)
				popBackStack()
			}
		}
		recycler.setHasFixedSize(true)
		recycler.adapter = adapter

		viewModel.data.onEach {
			adapter.submitList(it)
		}.launchIn(viewLifecycleOwner.lifecycleScope)
	}

	companion object {
		private val TAG = HistoryFragment::class.simpleName
		const val HIST_KEY = "history"
	}
}