package cz.gopas.kalkulacka.calc

import android.app.Application
import android.util.Log
import androidx.annotation.IdRes
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import androidx.room.Room
import cz.gopas.kalkulacka.R
import cz.gopas.kalkulacka.history.HistoryDatabase
import cz.gopas.kalkulacka.history.HistoryEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class CalcViewModel(app: Application) : AndroidViewModel(app) {
	private val prefs = viewModelScope.async(Dispatchers.IO) {
		PreferenceManager.getDefaultSharedPreferences(getApplication())
	}

	private val db = Room.databaseBuilder(getApplication(), HistoryDatabase::class.java, "history")
		.build()
		.historyDao()

	private val _res = MutableStateFlow("")
	val res: StateFlow<String> = _res

	private val _ans = MutableStateFlow("")
	val ans: StateFlow<String> = _ans

	suspend fun calc(a: Float, b: Float, @IdRes op: Int) {
		_res.value = when (op) {
			R.id.add -> a + b
			R.id.sub -> a - b
			R.id.mul -> a * b
			R.id.div -> a / b
			else -> Float.NaN
		}.toString().also { result ->
			viewModelScope.launch(Dispatchers.IO) {
				prefs.await().edit().putString(ANS_KEY, result).commit()
			}
			viewModelScope.launch(Dispatchers.IO) {
				try {
					db.add(HistoryEntity(result.toFloat()))
				} catch (t: Throwable) {
					Log.w(TAG, t)
				}
			}
		}
	}

	suspend fun ans() {
		_ans.value = prefs.await().getString(ANS_KEY, "")!!
	}

	private companion object {
		private val TAG = CalcViewModel::class.simpleName
		private const val ANS_KEY = "ans"
	}
}