package cz.gopas.kalkulacka.calc

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import cz.gopas.kalkulacka.R
import cz.gopas.kalkulacka.databinding.FragmentCalcBinding
import cz.gopas.kalkulacka.history.HistoryFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class CalcFragment : Fragment() {
	private var binding: FragmentCalcBinding? = null
	private val viewModel by viewModels<CalcViewModel>()

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	) = FragmentCalcBinding.inflate(inflater, container, false).also {
		binding = it
	}.root

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		binding?.run {
			viewModel.ans
				.onEach { aEdit.setText(it) }
				.launchIn(viewLifecycleOwner.lifecycleScope)

			viewModel.res
				.onEach { res.text = it }
				.launchIn(viewLifecycleOwner.lifecycleScope)

			calc.setOnClickListener {
				viewLifecycleOwner.lifecycleScope.launch {
					viewModel.calc(
						aEdit.text.toString().toFloatOrNull() ?: Float.NaN,
						bEdit.text.toString().toFloatOrNull() ?: Float.NaN,
						ops.checkedRadioButtonId
					)
				}
			}
			share.setOnClickListener { share() }
			ans.setOnClickListener {
				viewLifecycleOwner.lifecycleScope.launch {
					viewModel.ans()
				}
			}
			history.setOnClickListener(
				Navigation.createNavigateOnClickListener(
					CalcFragmentDirections.history()
				)
			)
			findNavController().currentBackStackEntry
				?.savedStateHandle
				?.getLiveData<Float>(HistoryFragment.HIST_KEY)
				?.observe(viewLifecycleOwner) {
					bEdit.setText("$it")
				}
		}
	}

	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)
		outState.putCharSequence(RES_KEY, binding?.res?.text)
	}

	override fun onViewStateRestored(savedInstanceState: Bundle?) {
		super.onViewStateRestored(savedInstanceState)
		binding?.res?.text = savedInstanceState?.getCharSequence(RES_KEY)
	}

	override fun onDestroyView() {
		binding = null
		super.onDestroyView()
	}

	private fun share() {
		Log.d(TAG, "Share")
		val intent = Intent(Intent.ACTION_SEND)
			.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, binding?.res?.text))
			.setType("text/plain")
		startActivity(intent)
	}

	private companion object {
		private val TAG = CalcFragment::class.simpleName
		private const val RES_KEY = "res"
	}
}