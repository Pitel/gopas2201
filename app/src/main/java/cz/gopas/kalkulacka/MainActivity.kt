package cz.gopas.kalkulacka

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController

class MainActivity : AppCompatActivity() {

	private lateinit var appBarConfiguration: AppBarConfiguration

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		val navHostFragment =
			supportFragmentManager.findFragmentById(R.id.container) as NavHostFragment
		val navController = navHostFragment.navController
		appBarConfiguration = AppBarConfiguration(navController.graph)
		setupActionBarWithNavController(navController, appBarConfiguration)

		Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "onCreate")
		Log.d(TAG, "${intent.dataString}")
	}

	override fun onSupportNavigateUp() =
		findNavController(R.id.container).navigateUp(appBarConfiguration)
				|| super.onSupportNavigateUp()

	override fun onResume() {
		super.onResume()
		Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "onResume")
	}

	override fun onPause() {
		Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show()
		Log.d(TAG, "onPause")
		super.onPause()
	}

	override fun onCreateOptionsMenu(menu: Menu?): Boolean {
		menuInflater.inflate(R.menu.menu, menu)
		return super.onCreateOptionsMenu(menu)
	}

	override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
		R.id.about -> {
			findNavController(R.id.container).navigate(NavGraphDirections.about("Jan Kaláb"))
			true
		}
		else -> super.onOptionsItemSelected(item)
	}

	private companion object {
		private val TAG = MainActivity::class.simpleName
	}
}