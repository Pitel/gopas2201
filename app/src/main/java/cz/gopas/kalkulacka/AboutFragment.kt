package cz.gopas.kalkulacka

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.google.android.material.textview.MaterialTextView

class AboutFragment : Fragment(R.layout.fragment_about) {

	private val args by navArgs<AboutFragmentArgs>()

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		(view as MaterialTextView).text = getString(R.string.about_text, args.name)
	}
}